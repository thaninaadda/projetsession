/*****************************************************
 * Fichier: connexion.js
 * Description: c'est le fichier javascript du site web
 * Date: 30 novembre
 * Auteur: Thanina Adda
 * version : 1.6
 */
//-------------------------------------------------------------------Script Pompe

//fonctions qui permet de réaliser la connexions dans la vue
function demandeConnexion() {
    //crée les parametres sous forme d'un seule chaine de caracteres
    $identifiant = document.getElementById("identifiant").value;
    $mdp = document.getElementById("motDePasse").value;

    console.log($identifiant);
    console.log($mdp);
    var xhttp = new XMLHttpRequest();
    console.log("debutLogin");
    xhttp.onreadystatechange = function () {
        if (xhttp.readyState === 4) {
            console.log("stateOK");
            console.log(xhttp.responseText);
            if(xhttp.responseText == "ok"){
              window.location=("/menu");
            }
          }else{
            console.log("statePASOK");
          }
    };

    xhttp.open("POST", "login", true);
    xhttp.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    var params = String("login") + String("=") + String($identifiant)+String("&")+String("mdp") + String("=") + String($mdp);
    xhttp.send(params);
}



