/**
    Gestion d'un serveur WEB
    @file MyServer.cpp
    @author Alain Dubé
    @version 1.1 20/11/20
    @version 1.2 30/11/21
*/
#include <Arduino.h>
#include "MyServer.h"
using namespace std;
typedef std::string (*CallbackType)(std::string);
CallbackType MyServer::ptrToCallBackFunction = NULL;

// Exemple pour appeler une fonction CallBack
// if (ptrToCallBackFunction) (*ptrToCallBackFunction)(stringToSend);
void MyServer::initCallback(CallbackType callback)
{
    ptrToCallBackFunction = callback;
}

void MyServer::initAllRoutes()
{
    currentTemperature = 3.3f;

    // Initialisation du SPIFF.
    if (!SPIFFS.begin(true))
    {
        Serial.println("An Error has occurred while mounting SPIFFS");
        return;
    }

    // Route initiale (page html)
    this->on("/menu", HTTP_GET, [](AsyncWebServerRequest *request)
             { request->send(SPIFFS, "/index_fr.html", "text/html"); });

    this->on("/", HTTP_GET, [](AsyncWebServerRequest *request)
             { request->send(SPIFFS, "/connexion.html", "text/html"); });

    // Route du script JavaScript
    this->on("/js/global.js", HTTP_GET, [](AsyncWebServerRequest *request)
             { request->send(SPIFFS, "/js/global.js", "text/javascript"); });

    this->on("/js/connexion.js", HTTP_GET, [](AsyncWebServerRequest *request)
             { request->send(SPIFFS, "/js/connexion.js", "text/javascript"); });

    this->on("/css/global.css", HTTP_GET, [](AsyncWebServerRequest *request)
             { request->send(SPIFFS, "/css/global.css", "text/css"); });

    this->on("/css/w3.css", HTTP_GET, [](AsyncWebServerRequest *request)
             { request->send(SPIFFS, "/css/w3.css", "text/css"); });

    this->on("/image/logo.png", HTTP_GET, [](AsyncWebServerRequest *request)
             { request->send(SPIFFS, "/image/logo.png", "image/png"); });

    this->onNotFound([](AsyncWebServerRequest *request)
                     { request->send(404, "text/plain", "Page Not Found"); });

    // envois le login et le mot de passe a l'api et recoit leurs validité
    this->on("/login", HTTP_POST, [](AsyncWebServerRequest *request)
             {
                 Serial.println("login... ");
                 String login = "";
                 String mdp = "";
                 if (request->hasParam("login", true))
                 {
                     login = request->getParam("login", true)->value();
                 }
                 if (request->hasParam("mdp", true))
                 {
                     mdp = request->getParam("mdp", true)->value();
                 }

                 HTTPClient http;

                 //String usersApiRestAddress = "http://172.16.206.221:3000/api/users/login";
                 String usersApiRestAddress = "http://192.168.102.235:3000/api/users/login";
            
                 http.begin(usersApiRestAddress);
                 http.addHeader("Accept", "application/json");
                 http.addHeader("Content-Type", "application/json");

                 String param = "{\"login\": \"" + login + "\",\"mdp\":\"" + mdp + "\"}";

                 http.POST(param);

                 String response = http.getString();

                 Serial.println(response);

                 request->send(200, "text/plain", response); });

    // renvoit la liste des bois

    this->on("/getAllWoodOptions", HTTP_GET, [](AsyncWebServerRequest *request)
             {
        Serial.println("getAllWoodOptions... ");

        HTTPClient http;
        //String woodApiRestAddress = "http://172.16.206.221:3000/api/woods";
         String woodApiRestAddress = "http://192.168.102.235:3000/api/woods";
        http.begin(woodApiRestAddress);
        http.GET();
        String response = http.getString();
        
        request->send(200, "text/plain", response); });


// récupere la temperature mesuré par l'esp
    this->on("/lireTemp", HTTP_GET, [](AsyncWebServerRequest *request)
             {
    std::string repString = "";
    if (ptrToCallBackFunction)  repString = (*ptrToCallBackFunction)("askTemperature");
   
    String lireTempDuFour =String(repString.c_str());
    
    request->send(200, "text/plain", lireTempDuFour ); });


 // envoit le signal de démarrage du four a l'esp
    this->on("/declencheFour", HTTP_POST, [](AsyncWebServerRequest *request)
             {
                 String tmp = "";
                 if (request->hasParam("temperature", true))
                 {
                     String temperature = request->getParam("temperature", true)->value();
                     tmp = temperature;
                 }
                 if (request->hasParam("duree", true))
                 {
                     String duree = request->getParam("duree", true)->value();
                     tmp = tmp + " " + duree;
                 }

                 tmp = "declencheFour " + tmp;

                 if (ptrToCallBackFunction)
                     (*ptrToCallBackFunction)(tmp.c_str());
                 request->send(204); });

 // récupere le status de l'esp (off/cold/heat)
    this->on("/lireStatus", HTTP_GET, [](AsyncWebServerRequest *request)
             {
                 std::string repString = "";

                 if (ptrToCallBackFunction)
                     repString = (*ptrToCallBackFunction)("askStatus");

                 String lireStatus = String(repString.c_str());

                 request->send(200, "text/plain", lireStatus); });

    //-------------------------------------------------------------------Formulaire connexion
    this->begin();
};
