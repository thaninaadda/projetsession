/* Copyright (C) 2021 Thanina Adda
 * All rights reserved.
 *
 * Projet Sac
 * Ecole du Web
 * Cours Objets connectés (c)2021
 *
    @file     main.cpp
    @author   Thanina Adda
    @version  1.1 21/08/15

    Historique des versions
           Version    Date       Auteur       Description
           0.7        30/11/2021  Thanina     Final

    platform = espressif32
    board = esp32doit-devkit-v1
    framework = arduino
    lib_deps =
            ESPAsyncWebServer-esphome
            bblanchon/ArduinoJson@^6.17.2
            adafruit/Adafruit SSD1306@^2.4.7
	        adafruit/Adafruit BusIO@^1.9.3
	        adafruit/Adafruit GFX Library@^1.10.12

Autres librairies (à copier dans le répertoire lib)
         WifiManager-master
         DHT
         myFunctions.cpp
         Adafruit-GFX-Library-master

            //Remarques
            //Pour trouver le WifiManager (dans la branche development)
            //   https://github.com/tzapu/WiFiManager/tree/development
            //   Ne pas oublier d'appuyez sur l'ampoule et choisir : ajouter Lib
           //WiFiManager - Arduino Reference ()


Fonctions utiles (utilitaires)
        /lib/MYLIB/myFunctions.cpp
            //Pour vérifier plus simplement que deux chaines sont identiques
            bool isEqualString(std::string line1, std::string line2)
            //Pour extraire une partie d'une chaine de caractères avec l'aide d'un index
            std::string getValue(std::string data, char separator, int index)
            //Pour remplacer plus facilement une sous chaine
            bool replaceAll(std::string& source, const std::string& from, const std::string& to)
            //Pour obtenir un chaine aléatoire d'une certaine longeur
            std::string get_random_string(unsigned int len)

    Arborescence des fichiers de la page web        

            /data                       V1.0        Répertoire qui contient les fichiers du site WEB
                index_fr.html           V1.0    Page index du site WEB
                connexion.html          V1.0   Page de connexion du site WEB
            /css:
                global.css              V1.0    CSS de index_fr.html
                w3.css                  V1.0    CSS de la connexion
            /js:
                connexion.js            V1.0    javascript permettant la connexion
                global.js               V1.0    javascript permettant l'utilisation de l'objet connecté
            /image:
                logo.png                V1.0 image de la vue.

            Arborescence des classes systemes:

            /DHT:
            DHT                        V1.0 Senseur de temperature
            /MYLIB
            myFunctions.cpp            V1.0 Fonctions utilitaires pour le projet

            MyOled                      Gestion d'un écran Oled Utilisant un GPIO d'un contrôleur
            MyOledView                  
            MyOledViewErrorWifiConnexion     
            MyOledViewInitialisation                     
            MyOledViewWifiAp 
            MyOledViewWorking
            MyOledViewWorkingOFF
            MyOledViewWorkingCold
            MyOledViewWorkingHeat

            /src:
            
            main.cpp                   V1.0  Code principal
            MyServer                   V1.0  Gestion d'un serveur WEB
            TemperatureStub            V1.0  Gestion de la température du Four (STUB)
            MyButtons                  V1.0  Gestion d'un bouton pour permettre au système de démarrer 

            

 * */

// les includes du main
#include <iostream>
#include <string>

#include <Arduino.h>
#include <ArduinoJson.h>
#include "myFunctions.cpp" //fonctions utilitaires (get_random_string)
using namespace std;


// Pour la gestion du WIFI Manager
#include <HTTPClient.h>
#include <WiFiManager.h>
WiFiManager wm;
#define WEBSERVER_H

// Pour la gestion du serveur ESP32
#include "MyServer.h"
MyServer *myServer = NULL;

// pour l'affichage Oled

#include <Wire.h>
#include <MyOled.h>
#include <MyOledView.h>
#include <MyOledViewWifiAp.h>
#include <MyOledViewErrorWifiConnexion.h>
#include <MyOledViewWorking.h>
#include <MyOledViewInitialisation.h>
#include <MyOledViewWorking.h>
#include <MyOledViewWorkingOFF.h>
#include <MyOledViewWorkingCold.h>
#include <MyOledViewWorkingHeat.h>


// initlisations des objets Oled
MyOled *myOled = new MyOled(&Wire);
MyOledViewWifiAp *myOledViewWifiAp = new MyOledViewWifiAp();
MyOledViewErrorWifiConnexion *myOledViewErrorWifiConnexion = new MyOledViewErrorWifiConnexion();
MyOledViewInitialisation *myOledViewInitialisation = new MyOledViewInitialisation();

MyOledViewWorking *myOledViewWorking = new MyOledViewWorking();
MyOledViewWorkingOFF *myOledViewWorkingOff = new MyOledViewWorkingOFF();
MyOledViewWorkingCold *myOledViewWorkingCold = new MyOledViewWorkingCold();
MyOledViewWorkingHeat *myOledViewWorkingHeat = new MyOledViewWorkingHeat();

// Variable pour la connection Wifi
const char *SSID = "SAC_Thanina";
const char *PASSWORD = "sac123456";
String ssIDRandom;

// pour l'affichage
#define SCREEN_WIDTH 128      // taille de l'écran en longeur, en pixel
#define SCREEN_HEIGHT 64      // taille de l'écran en largeur, en pixel
#define OLED_RESET 4          // Reset pin # (or -1 if sharing Arduino reset pin)
#define OLED_I2C_ADDRESS 0x3C // Adresse I2C de l'écran Oled
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, -1);
int frame_delay = 15;

// Pour avoir les données du senseur de température
#include "TemperatureStub.h"
#define DHTPIN 15     // Pin utilisée par le senseur DHT22 / DHT22
#define DHTTYPE DHT22 // Le type de senseur utilisé (mais ce serait mieux d'avoir des DHT22 pour plus de précision)
TemperatureStub *temperatureStub = NULL;

// pour passer la temperature reçue en std::string
#include <sstream>

// Pour la gestion des boutons
#include "MyButton.h"
MyButton *myButtonAction = NULL;
MyButton *myButtonReset = NULL;

// valeur qui est affichée sur l'écran
float valueToPrint;

// permet de savoir si le Four est en marche
boolean demarre = false;
// permet à la page web de savoir quel est le status de l'esp
string status = "null";

// valeurs envoyées par le service web
string temperature = "null";
string duree = "null";
float temperatureFloat;
int dureeInt;

// Définition des trois leds de statut
#define GPIO_PIN_LED_LOCK_ROUGE 12 // GPIO12
#define GPIO_PIN_LED_MILD_BLUE 14  // GPIO14
#define GPIO_PIN_LED_OK_GREEN 27   // GPIO27

// fonction statique qui permet aux objets d'envoyer des messages (callBack)
//   arg0 : Action
//  arg1 ... : Parametres
std::string CallBackMessageListener(string message)
{

    while (replaceAll(message, std::string("  "), std::string(" ")))
        ;
    // Décortiquer le message
    string actionToDo = getValue(message, ' ', 0);
    string arg1 = getValue(message, ' ', 1);
    string arg2 = getValue(message, ' ', 2);
    string arg3 = getValue(message, ' ', 3);
    string arg4 = getValue(message, ' ', 4);
    string arg5 = getValue(message, ' ', 5);
    string arg6 = getValue(message, ' ', 6);
    string arg7 = getValue(message, ' ', 7);
    string arg8 = getValue(message, ' ', 8);
    string arg9 = getValue(message, ' ', 9);
    string arg10 = getValue(message, ' ', 10);

    // renvois la temperature actuelle a l'esp
    std::string temperature = "0.0";
    if (string(actionToDo.c_str()).compare(string("askTemperature")) == 0)
    {
        std::ostringstream ss;
        ss << temperatureStub->getTemperature();

        std::string s(ss.str());
        valueToPrint = temperatureStub->getTemperature();
        return s;
    }
    //permet de savoir que le four doit être lancé
    if (string(actionToDo.c_str()).compare(string("declencheFour")) == 0)
    {
        demarre = true;

        Serial.println(arg1.c_str());
        Serial.println(arg2.c_str());
        temperature = arg1.c_str();
        istringstream(temperature) >> temperatureFloat;
        duree = arg2.c_str();
        istringstream(duree) >> dureeInt;

        Serial.println("four lancé");
        return "four lancé";
    }

    // fonctions qui renvois le statut actuel du four ( off, cold, heat)
    if (string(actionToDo.c_str()).compare(string("askStatus")) == 0)
    {
        return (status.c_str());
    }

    if (string(actionToDo.c_str()).compare(string("action")) == 0)
    {
        return (String("Ok").c_str());
    }

    std::string result = "";
    return result;
}


// setup du main
void setup()
{
    Serial.begin(9600);
    delay(100);

    // ----------- Initialisation des LED statuts ----------------
    pinMode(GPIO_PIN_LED_LOCK_ROUGE, OUTPUT);
    pinMode(GPIO_PIN_LED_OK_GREEN, OUTPUT);
    pinMode(GPIO_PIN_LED_MILD_BLUE, OUTPUT);

    // ----------- initialisation de MyOled ----------------

    myOled->init(OLED_I2C_ADDRESS, false);
    myOled->veilleDelay(30); // En secondes

    
    myOledViewWorking->init("id : 123456");

    myOledViewInitialisation->setNomDuSysteme("INITILISATION");
    myOledViewInitialisation->setIdDuSysteme("id : ID3944");
    myOledViewInitialisation->setSensibliteBoutonActif("Bouton ACTION: ???");
    myOledViewInitialisation->setSensibliteBoutonReset("Bouton RESET: ???");

    myOledViewWifiAp->setNomDuSysteme("Initialisation Wifi");
    myOledViewWifiAp->setSslDuSysteme("Ip : ???");
    myOledViewWifiAp->setPassDuSysteme("mdp : ???");

    myOledViewWorkingOff->init("id : 12345");
    myOledViewWorkingCold->init("id : 12345");
    myOledViewWorkingHeat->init("id : 12345");

    myOled->displayView(myOledViewInitialisation);

    ////  ---------------- Initiation pour la lecture de la température----------------
    temperatureStub = new TemperatureStub;
    temperatureStub->init(DHTPIN, DHTTYPE); // Pin 15 et Type DHT22

    // ----------------Gestion des boutons----------------
    myButtonAction = new MyButton(); // Pour lire le bouton actions
    myButtonAction->init(T8);
    int sensibilisationButtonAction = myButtonAction->autoSensibilisation();

    myButtonReset = new MyButton(); // Pour lire le bouton hard reset
    myButtonReset->init(T9);
    int sensibilisationButtonReset = myButtonReset->autoSensibilisation();

    Serial.print("sensibilisation du Button Reset : ");
    Serial.println(sensibilisationButtonReset);

    // envoie les nouvelles infos de sensibilité à l'oled
    std::ostringstream ss;
    ss << sensibilisationButtonAction;

    myOledViewInitialisation->setSensibliteBoutonActif("Bouton ACTION: " + ss.str());

    std::ostringstream tt;
    tt << sensibilisationButtonReset;

    myOledViewInitialisation->setSensibliteBoutonReset("Bouton RESET: " + tt.str());

    myOled->updateCurrentView(myOledViewInitialisation);
    delay(2000);

    myOled->updateCurrentView(myOledViewWifiAp);

    // ----------------Connection au WifiManager----------------
    String ssIDRandom, PASSRandom;
    String stringRandom;
    stringRandom = get_random_string(4).c_str();
    ssIDRandom = SSID;
    ssIDRandom = ssIDRandom + stringRandom;
    stringRandom = get_random_string(4).c_str();
    PASSRandom = PASSWORD;
    PASSRandom = PASSRandom + stringRandom;

    char strToPrint[128];

    sprintf(strToPrint, "Identification : %s   MotDePasse: %s", ssIDRandom.c_str(), PASSRandom.c_str());
    Serial.println("ici");

    Serial.println(strToPrint);

    //on declenche l'affichage de l'erreur ici car on ne rentre jamais dans le if
    myOledViewErrorWifiConnexion->setParams("ssid", ssIDRandom.c_str());
    myOledViewErrorWifiConnexion->setParams("pass", PASSRandom.c_str());
    myOled->displayView(myOledViewErrorWifiConnexion);
    if (!wm.autoConnect(ssIDRandom.c_str(), PASSRandom.c_str()))
    {
        Serial.println("Erreur de connexion.");
    }
    else
    {
        Serial.println("Connexion Établie:");
        myOledViewWifiAp->setNomDuSysteme("Connexion Établie:");
        myOledViewWifiAp->setSslDuSysteme((WiFi.localIP()).toString().c_str());
        myOledViewWifiAp->setPassDuSysteme(PASSRandom.c_str());
        myOledViewWorkingOff->setParams("SslDuSysteme", (WiFi.localIP()).toString().c_str());
        myOledViewWorkingCold->setParams("SslDuSysteme", (WiFi.localIP()).toString().c_str());
        myOledViewWorkingHeat->setParams("SslDuSysteme", (WiFi.localIP()).toString().c_str());
        myOled->updateCurrentView(myOledViewWifiAp);
    
        delay(5000);
    }
    // ----------- Routes du serveur ----------------
    myServer = new MyServer(80);
    myServer->initAllRoutes();
    myServer->initCallback(&CallBackMessageListener);

    myOled->displayView(myOledViewWorking);

} // setup

// loop du main
void loop()
{
    // -----------Gestion du bouton Action-----------
    int buttonAction = myButtonAction->checkMyButton();
    if (buttonAction > 1)
    { // Si appuyé plus de 0.1 secondes
        Serial.println("action");
    }

    // -----------Gestion du bouton Reset-----------
    int buttonReset = myButtonReset->checkMyButton();
    if (buttonReset > 2)
    { // Si appuyé plus de 0.1 secondes
        Serial.println("reset");
        ESP.restart();
    }

    float temperatureTMP = temperatureStub->getTemperature();
    std::ostringstream vv;
    vv << temperatureTMP;
    myOledViewWorkingOff->setParams("temperature", vv.str());
    myOledViewWorkingCold->setParams("temperature", vv.str());
    myOledViewWorkingHeat->setParams("temperature", vv.str());

    //tant que le four ne démarre pas, on affiche l'etat off
    if (!demarre)
    {
        status = "off";
        myOled->displayView(myOledViewWorkingOff);
        digitalWrite(GPIO_PIN_LED_LOCK_ROUGE, LOW);
        digitalWrite(GPIO_PIN_LED_MILD_BLUE, LOW);
        digitalWrite(GPIO_PIN_LED_OK_GREEN, LOW);
        digitalWrite(GPIO_PIN_LED_OK_GREEN, HIGH);
        delay(70);
    }
    else
    {
        //si le four est lancé, on verifie la temperature
        if (temperatureTMP < temperatureFloat)
        {
            status = "cold";
            myOled->updateCurrentView(myOledViewWorkingCold);
            digitalWrite(GPIO_PIN_LED_LOCK_ROUGE, LOW);
            digitalWrite(GPIO_PIN_LED_MILD_BLUE, LOW);
            digitalWrite(GPIO_PIN_LED_OK_GREEN, LOW);
            digitalWrite(GPIO_PIN_LED_MILD_BLUE, HIGH);
        }
        else
        {
            //si la temperature est bonne, on declenche une boucle qui se termine à la fin du timer (ou si la temperature a trop baissée)
            status = "heat";
            int timer = dureeInt + 1;
            do
            {
                timer = timer - 1;
                Serial.print(timer);
                myOled->updateCurrentView(myOledViewWorkingHeat);
                digitalWrite(GPIO_PIN_LED_LOCK_ROUGE, LOW);
                digitalWrite(GPIO_PIN_LED_MILD_BLUE, LOW);
                digitalWrite(GPIO_PIN_LED_OK_GREEN, LOW);
                digitalWrite(GPIO_PIN_LED_LOCK_ROUGE, HIGH);
                delay(1000);

            } while (temperatureTMP >= temperatureFloat && timer > 0);
            // quand le chauffage est terminé, le four doit s'arreter
            demarre = false;
        }
    }

    delay(1000);

} // loop
