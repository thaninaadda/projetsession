/**
    Gestion d'un écran Oled Utilisant un GPIO d'un contrôleur
    @file MyOledView.cpp
    @author Thanina Adda
    @version 1.1 30/11/21
*/
#include <Arduino.h>
#include "MyOledView.h"
using namespace std;

/**
 * setParams Affiche un caractère spécial sur le Oled
 *
 * Affiche  un caractère spécial sur le Oled (é ou o)
 *
 * @param string
 * @param string
 */

void MyOledView::setParams(string tag, string value)
{
    int tagPos = findTag(tag);
    if (tagPos == 0)
    {
        params.push_back(tag);
        params.push_back(value);
        return;
    }
    params[tagPos] = value;
}

/**
 * getTag
 *
 *
 *
 * @param string
 */
string MyOledView::getTag(string tag)
{
    int tagPos = findTag(tag);
    if (tagPos != 0)
        return (params[tagPos]);
    return ("");
}

/**
 * getTag
 *
 *
 *
 * @param string
 */
int MyOledView::findTag(string tag)
{
    for (unsigned i = 0; i < params.size(); i += 2)
    {
        if (string(params[i].c_str()).compare(tag) == 0)
        {
            return (i + 1);
        }
    }
    return (0);
}

/**
 * init, initialisation dans MyOledView
 *
 *
 *
 * @param string
 */
void MyOledView::init(std::string _id)
{

    Serial.println("Init dans MyOledView");
    myId = _id;
    params.clear();
}

std::string MyOledView::id()
{
    return (myId);
}